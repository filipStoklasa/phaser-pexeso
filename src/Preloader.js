import backgroundMain from "./assets/bg_deck_select_2.png"
import deckActivities from "./assets/deck_activities.png"
import deckCharacters from "./assets/deck_characters.png"
import deckMoods from "./assets/deck_moods.png"
import bg_game_2 from "./assets/bg_game_2.png"
import card_back from "./assets/cards/card_back.png"
import card_back_p1 from "./assets/cards/card_back_p1.png"
import card_back_p2 from "./assets/cards/card_back_p2.png"
import gameTutorial1 from "./assets/sound/gameTutorial1.mp3"
import gameTutorial2 from "./assets/sound/gameTutorial2.mp3"
import gameTutorial3 from "./assets/sound/gameTutorial3.mp3"
import cake from "./assets/counter/counter_cake.png"
// import line from "./assets/counter/counter_line.png"

import ll from "./assets/counter/el_l.png"
import lr from "./assets/counter/el_r.png"

import counter_cyclop from "./assets/counter/counter_cyclop.png"
import counter_reindeer from "./assets/counter/counter_reindeer.png"
import goCyclops from "./assets/sound/cyclops_turn.mp3"
import goCyclops2 from "./assets/sound/cyclops_turn2.mp3"
import goReindeers from "./assets/sound/reindeers_turn.mp3"
import goReindeers2 from "./assets/sound/reindeers_turn2.mp3"

export default class Preloader extends Phaser.Scene {
    constructor() {
        super("Preloader")
    }

    preload() {
        this.load.image("bgMain", backgroundMain)
        this.load.image("deck1", deckActivities)
        this.load.image("deck2", deckCharacters)
        this.load.image("deck3", deckMoods)
        this.load.image("bgGame", bg_game_2)
        this.load.image("card_back", card_back)
        this.load.image("card_back_p1", card_back_p1)
        this.load.image("card_back_p2", card_back_p2)
        this.load.image("cake", cake)
        // this.load.image("line", line)
        this.load.image("ll", ll)
        this.load.image("lr", lr)
        this.load.image("counter_cyclop", counter_cyclop)
        this.load.image("counter_reindeer", counter_reindeer)

        this.load.audio("gameTutorial1", gameTutorial1)
        this.load.audio("gameTutorial2", gameTutorial2)
        this.load.audio("gameTutorial3", gameTutorial3)
        this.load.audio("goCyclops", goCyclops)
        this.load.audio("goCyclops2", goCyclops2)
        this.load.audio("goReindeers", goReindeers)
        this.load.audio("goReindeers2", goReindeers2)

        this.load.setPath("/src/assets/animations/")
        this.load.spine("intro_hd", "intro.json", ["intro_sd.atlas"], true)
        this.load.spine("reindeer", "player_reindeer.json", ["player_reindeer_sd.atlas"], true)
        this.load.spine("cyclop", "player_cyclop.json", ["player_cyclop_sd.atlas"], true)
        this.load.spine("pexeso", "win_fail.json", ["pexeso_full.atlas"], true)
    }

    create() {
        this.scene.start("MenuScene")
    }
}
