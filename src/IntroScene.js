// import SpinePlugin from "phaser/plugins/spine/dist/SpineCanvasPlugin"
// import introJSON from "./assets/animations/intro.json"
// import introATLAS from "./assets/animations/intro_sd.atlas"

export default class IntroScene extends Phaser.Scene {
    constructor() {
        super("IntroScene")
    }
    init(data) {
        this._world = data.world
        this.x = this.game.config.width / 100
        this.y = this.game.config.height / 100
        this._bgRatio = 1570 / 728
        this._calculate = this._bgRatio * (this.y * 100)
        this._scaledSize = ~~((this.y * 100 * 100) / 1570) / 100
        this._intro = this.add.spine(this.x * 50, this.y * 50, "intro_hd", "intro", false).setScale((this.y * 100) / (728 * 2))
        this._intro.state.listeners[0].complete = this.endAnim()
    }
    create() {
        const overlay = this.make.graphics({ x: 0, y: 0, add: false })
        overlay.fillStyle(" 0xffffff", 0)
        overlay.fillRect(0, 0, this.x * 100, this.y * 100)
        overlay.generateTexture("overlay", this.x * 100, this.y * 100)
        this.overlay = this.add
            .image(0, 0, "overlay")
            .setOrigin(0, 0)
            .on("pointerdown", this.endAnim())
            .setInteractive()

        const fadeIn = this.make.graphics({ x: 0, y: 0, add: false })
        fadeIn.fillStyle(" 0xffffff", 1)
        fadeIn.fillRect(0, 0, this.x * 100, this.y * 100)
        fadeIn.generateTexture("fadeIn", this.x * 100, this.y * 100)
        this.fadeIn = this.add
            .image(0, 0, "fadeIn")
            .setOrigin(0, 0)
            .setAlpha(0)
            .on("pointerdown", this.endAnim())
            .setInteractive()
    }
    endAnim() {
        return () => {
            this._animationOnExit()
        }
    }
    _animationOnExit() {
        let animOnExit = this.tweens.createTimeline()
        animOnExit.add({
            targets: this.fadeIn,
            ease: "Power1",
            duration: 1000,
            repeat: 0,
            alpha: 1,
            onComplete: () => {
                this._intro.destroy()
                this._intro = null
                this.fadeIn.destroy()
                this.overlay.destroy()
                this.scene.start("GameScene", { world: this._world })
            }
        })
        animOnExit.play()
    }
}
