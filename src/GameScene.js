import GridGenerator from "./utils/gridGenerator"
import Card from "./utils/Card"
import { soundImporter } from "./utils/imports"

export default class GameScene extends Phaser.Scene {
    constructor() {
        super("GameScene")
    }
    init(data) {
        this._p = this.game.players
        this._world = data.world
        this.x = this.game.config.width / 100
        this.y = this.game.config.height / 100
        this.GRID_SIZES = this._p._round < 2 ? { x: 4, y: 2 } : { x: 4, y: 3 }
        this._import = new GridGenerator(this.GRID_SIZES, this._world)
        this._deck = this._shuffleCards(this._generateCards(this._import.composition))
        this._selectedCouple = []
        this._completed = []
        this._turns = 0
        this._sounds = soundImporter(this._world)

        const fadeIn = this.make.graphics({ x: 0, y: 0, add: false })
        fadeIn.fillStyle(" 0xffffff", 1)
        fadeIn.fillRect(0, 0, this.x * 100, this.y * 100)
        fadeIn.generateTexture("fadeIn", this.x * 100, this.y * 100)

        this.add
            .image(this.x * 50, this.y * 50, "bgGame")
            .setOrigin(0.5, 0.5)
            .setDisplaySize((3328 / 1536) * (this.y * 100), this.y * 100)

        this._cyclop = this.add //done
            .spine(0, this.y * 100, "cyclop", "idle", true)
            .setScale(0.4)
            .setDepth(1000)

        this._reindeer = this.add //done
            .spine(this.x * 100, this.y * 100, "reindeer", "idle", true)
            .setScale(0.4)
            .setDepth(1000)
    }

    preload() {
        for (let i in Object.keys(this._import.composition)) {
            this.load.audio(`${Object.keys(this._import.composition)[i]}`, this._sounds[Object.keys(this._import.composition)[i]])
            this.load.image(`${Object.keys(this._import.composition)[i]}`, Object.values(this._import.composition)[i])
        }
    }

    create() {
        this._imgRef = {} //done
        this._soundRef = {} //done
        this._lineSize = 742
        this._tutorial1 = this.sound.add("gameTutorial1") //done
        this._tutorial2 = this.sound.add("gameTutorial2") //done
        this._tutorial3 = this.sound.add("gameTutorial3") //done
        this._goCyclops = this.sound.add("goCyclops") //done
        this._goReindeer = this.sound.add("goReindeers") //done
        let _imgWidth = (this.game.config.width * 0.7) / this._import._gridWidth - 10
        let _imgHeight = (this.game.config.height * 0.85) / this._import._gridHeight - 10
        let imgSize = Math.min(_imgWidth, _imgHeight)
        let marginLeft = (this.x * 100 - this._import._gridWidth * (imgSize + 10)) / 2 + imgSize / 2
        let marginTop = (this.y * 100 - this._import._gridHeight * (imgSize + 10)) / 2 + imgSize / 2
        let k = 0

        this.fadeIn = this.add
            .image(0, 0, "fadeIn")
            .setOrigin(0, 0)
            .setAlpha(0)
            .setDepth(15000)

        //Progress at the end of each round
        this._cake = this.add
            .image(this.x * 50, this.y * 40, "cake")
            .setAlpha(0)
            .setScale(0.5)

        this._progressLineLeft = this.add
            .image(this.x * 50, this.y * 70, "ll")
            .setAlpha(0)
            .setDisplaySize(this.x * 35, (this.x * 35) / (742 / 98))
            .setOrigin(1, 0.5)

        this._progressLineRight = this.add
            .image(this.x * 50, this.y * 70, "lr")
            .setAlpha(0)
            .setOrigin(0, 0.5)
            .setDisplaySize(this.x * 35, (this.x * 35) / (742 / 98))

        this._counter_cyclop = this.add
            .image(this.x * 15, this.y * 70, "counter_cyclop")
            .setAlpha(0)
            .setDisplaySize((411 / 475) * this.y * 30, this.y * 30)

        this._counter_reindeer = this.add
            .image(this.x * 85, this.y * 70, "counter_reindeer")
            .setAlpha(0)
            .setDisplaySize((252 / 415) * this.y * 30, this.y * 30)

        this._initProgressCyclop()
        this._initProgressReindeer()
        //

        for (let i = 0; i < this._import._gridWidth; i++) {
            for (let j = 0; j < this._import._gridHeight; j++) {
                let itemName = k + this._deck[k]
                this._imgRef[itemName] = this.children.add(new Card(this, { src: "card_back", x: marginLeft + 5 + i * imgSize + i * 10, y: marginTop + 5 + j * imgSize + j * 10, imgSize }))
                this._imgRef[itemName].cardName = this._deck[k]
                this._soundRef[this._deck[k]] = this.sound.add(this._deck[k])
                k++
            }
        }

        if (this._p._round == 0) {
            this._playTutorial()
        } else {
            this._changePlayer()
        }
    }

    _turnBegin() {
        if (this._p._currentPlayer == 0) {
            this._goCyclops.play()
            this.tweens.add({
                targets: this._cyclop,
                x: 0,
                duration: 500,
                delay: 0
            })
            this.tweens.add({
                targets: this._reindeer,
                x: this.x * 120,
                duration: 500,
                delay: 0,
                onComplete: () => this._enableAllButtons()
            })
        } else {
            this._goReindeer.play()
            this.tweens.add({
                targets: this._reindeer,
                x: this.x * 100,
                duration: 500,
                delay: 0
            })
            this.tweens.add({
                targets: this._cyclop,
                x: -this.x * 20,
                duration: 500,
                delay: 0,
                onComplete: () => this._enableAllButtons()
            })
        }
    }

    _changePlayer() {
        this._disableAllButtons()
        this._p.changePlayer()
        this._cardsShift()
        this._turnBegin()
    }

    _shuffleCards(array) {
        var currentIndex = array.length,
            temporaryValue,
            randomIndex

        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex)
            currentIndex -= 1
            temporaryValue = array[currentIndex]
            array[currentIndex] = array[randomIndex]
            array[randomIndex] = temporaryValue
        }

        return array
    }

    _generateCards(deck) {
        let res = []
        for (let i in Object.keys(deck)) {
            res.push(Object.keys(deck)[i])
        }
        return [...res, ...res]
    }

    _onPressCard(item) {
        return () => {
            if (this._completed.indexOf(item.cardName) == -1) {
                item.disableInteractive()
                if (this._selectedCouple.length == 0) {
                    item.played = true
                    this._selectedCouple.push(item)
                    item.flipCard()
                } else if (!item.played && this._selectedCouple.length == 1) {
                    item.disableInteractive()
                    this._disableAllButtons()
                    item.played = true
                    this._selectedCouple.push(item)
                    item.flipCard()
                    this.time.delayedCall(600, this._resetTurn, [], this)
                }
            }
        }
    }

    _resetTurn() {
        if (this._selectedCouple[0].cardName == this._selectedCouple[1].cardName) {
            this._completed.push(this._selectedCouple[0].cardName)
            this._turns++
            let rotate = ~~(Math.random() * 10)
            this._p.setScore()
            for (let i in this._selectedCouple) {
                this._selectedCouple[i].setDepth(this._turns)
                this._selectedCouple[i].win(i == 0, rotate)
            }
            this._winAnimation()
            this._checkStatus()
        } else {
            for (let i in this._selectedCouple) {
                this._selectedCouple[Number(i)].flipBack()
                this._selectedCouple[Number(i)].played = false
            }
            this._selectedCouple = []
            this._looseAnimation()
        }
    }

    _checkStatus() {
        if (this._turns == this._import._amount) {
            let winner = this._p.endOfRound()
            this._setProgressState(winner)
        }
    }

    _enableAllButtons() {
        let k = 0
        for (let i = 0; i < this._import._gridWidth; i++) {
            for (let j = 0; j < this._import._gridHeight; j++) {
                let itemName = k + this._deck[k]
                this._imgRef[itemName].on("pointerdown", this._onPressCard(this._imgRef[itemName])).setInteractive()
                k++
            }
        }
    }

    _disableAllButtons() {
        for (let i in Object.values(this._imgRef)) {
            Object.values(this._imgRef)[i].disableInteractive()
        }
    }

    _cardsShift() {
        let toChange = Object.values(this._imgRef).filter(item => !item.played)
        let switchingCards = this.tweens.createTimeline()
        switchingCards.add({
            targets: toChange,
            ease: "Power1",
            duration: 400,
            repeat: 0,
            delay: 0,
            alpha: 0.3,
            onComplete: () => {
                for (let i in toChange) {
                    toChange[i].setTexture(this._p._currentPlayer == 0 ? "card_back_p1" : "card_back_p2")
                }
            }
        })
        switchingCards.add({
            targets: toChange,
            ease: "Power1",
            duration: 400,
            repeat: 0,
            delay: 0,
            alpha: 1
        })
        switchingCards.play()
    }

    _playTutorial() {
        this._tutorial1.on("complete", () => {
            this._tutorial2.play()
            this._reindeer.play("win", false)
            this._reindeer.state.listeners[0].complete = () => {
                this._reindeer.play("idle", true)
            }
            this._cyclop.play("win", false)
            this._cyclop.state.listeners[0].complete = () => {
                this._cyclop.play("idle", true)
            }
        })
        this._tutorial2.on("complete", () => {
            this._tutorial3.play()
            this._cardsShiftTutorial()
            this.time.delayedCall(800, this._cardsShiftTutorial, [], this)
        })
        this._tutorial3.on("complete", () => {
            this._turnBegin()
        })
        this._tutorial1.play()
    }
    _cardsShiftTutorial() {
        let switchingCards = this.tweens.createTimeline()
        switchingCards.add({
            targets: [...Object.values(this._imgRef)],
            ease: "Power1",
            duration: 400,
            repeat: 0,
            delay: 0,
            alpha: 0.3,

            onComplete: () => {
                this._p.changePlayer()
                for (let i in Object.values(this._imgRef)) {
                    Object.values(this._imgRef)[i].setTexture(this._p._currentPlayer == 0 ? "card_back_p1" : "card_back_p2")
                }
            }
        })
        switchingCards.add({
            targets: [...Object.values(this._imgRef)],
            ease: "Power1",
            duration: 400,
            repeat: 0,
            delay: 0,
            alpha: 1
        })
        switchingCards.play()
    }
    _winAnimation() {
        if (this._p._currentPlayer == 0) {
            this._cyclop.play("win", false)
            this._cyclop.state.listeners[0].complete = () => {
                this._cyclop.play("idle", true)
            }
        } else {
            this._reindeer.play("win", false)
            this._reindeer.state.listeners[0].complete = () => {
                this._reindeer.play("idle", true)
            }
        }
    }

    _looseAnimation = () => {
        if (this._p._currentPlayer == 0) {
            this._cyclop.play("fail", false)
            this._cyclop.state.listeners[0].complete = () => {
                this._changePlayer()
                this._cyclop.state.listeners[0].complete = null
                this._cyclop.play("idle", true)
            }
        } else {
            this._reindeer.play("fail", false)
            this._reindeer.state.listeners[0].complete = () => {
                this._reindeer.state.listeners[0].complete = null
                this._changePlayer()
                this._reindeer.play("idle", true)
            }
        }
    }
    _setProgressState(winner) {
        let showProgress = this.tweens.createTimeline()
        showProgress.add({
            targets: this._p._currentPlayer == 0 ? this._cyclop : this._reindeer,
            ease: "Power1",
            duration: 1000,
            repeat: 0,
            delay: 400,
            x: this._p._currentPlayer == 0 ? -(this.x * 50) : this.x * 150
        })
        showProgress.add({
            targets: [this._cake, this._progressLineLeft, this._progressLineRight, this._counter_cyclop, this._counter_reindeer],
            ease: "Power1",
            duration: 400,
            repeat: 0,
            delay: 400,
            alpha: 1,
            onComplete: () => {
                this._playProgress(winner)
                if (!this._p.weHaveWinner()) {
                    this.tweens.add({
                        targets: this.fadeIn,
                        ease: "Power1",
                        duration: 400,
                        repeat: 0,
                        delay: 3200,
                        alpha: 1,
                        onStart: () => {
                            this._destroy()
                        },
                        onComplete: () => {
                            if (!this._p.weHaveWinner()) {
                                this.scene.start("GameScene", { world: this._world })
                            }
                        }
                    })
                } else {
                    this._destroy()
                    this._playWinnerAnimation()
                }
            }
        })

        showProgress.play()
    }
    _playWinnerAnimation() {
        let winnerIs = Object.values(this._p._score).filter(item => item.wins > 3)
        if (winnerIs.length == 2) {
            this._winScene = this.add.spine(this.x * 50, this.y * 50, "pexeso", "colour_orange", false).setDepth(16000)
            this._winScene
                .setSkin(null)
                .setSkinByName("pexeso3")
                .setAnimation(1, "win", false)
            this._winScene.state.listeners[0].complete = () => {
                this._p.resetInstance()
                this.scene.start("MenuScene", { skip: true })
            }
        } else {
            if (winnerIs[0].name == "Player1") {
                this._winScene = this.add.spine(this.x * 50, this.y * 50, "pexeso", "colour_blue", false).setDepth(16000)
                this._winScene
                    .setSkin(null)
                    .setSkinByName("pexeso1")
                    .setAnimation(1, "win", false)
                this._winScene.state.listeners[0].complete = () => {
                    this._p.resetInstance()
                    this.scene.start("MenuScene", { skip: true })
                }
            } else {
                this._winScene = this.add.spine(this.x * 50, this.y * 50, "pexeso", "colour_green", false).setDepth(16000)
                this._winScene
                    .setSkin(null)
                    .setSkinByName("pexeso2")
                    .setAnimation(1, "win", false)
                this._winScene.state.listeners[0].complete = () => {
                    this._p.resetInstance()
                    this.scene.start("MenuScene", { skip: true })
                }
            }
        }
    }
    _playProgress(winner) {
        if (winner == "Player1") {
            this._progressCyclop()
        } else if (winner == "Player2") {
            this._progressReindeer()
        } else {
            this._progressCyclop()
            this._progressReindeer()
        }
    }
    _progressCyclop() {
        let left = this._p._score[0].wins
        let picAmount = (this.x * 35) / 4
        let cropAmount = (this._lineSize / 4) * left
        let moveTimeline = this.tweens.createTimeline()
        moveTimeline.add({
            targets: this._counter_cyclop,
            ease: "Sine.easeOut",
            duration: 300,
            delay: 1000,
            repeat: 0,
            x: this._counter_cyclop.x + picAmount / 2,
            onStart: () => {
                this.tweens.add({
                    targets: this._counter_cyclop,
                    ease: "Sine.easeOut",
                    duration: 300,
                    delay: 0,
                    repeat: 0,
                    yoyo: true,
                    y: this._counter_cyclop.y - this._counter_cyclop.displayHeight / 2
                })
                this._progressLineLeft.setCrop(cropAmount, 0, 742, 98)
            }
        })
        moveTimeline.add({
            targets: this._counter_cyclop,
            ease: "Sine.easeOut",
            duration: 600,
            delay: 0,
            repeat: 0,
            x: this._counter_cyclop.x + picAmount
        })
        moveTimeline.play()
    }

    _progressReindeer() {
        let right = this._p._score[1].wins
        let picAmount = (this.x * 35) / 4
        let cropAmount = (this._lineSize / 4) * right
        let moveTimeline = this.tweens.createTimeline()

        moveTimeline.add({
            targets: this._counter_reindeer,
            ease: "Sine.easeOut",
            duration: 300,
            delay: 1000,
            repeat: 0,
            x: this._counter_reindeer.x - picAmount / 2,
            onStart: () => {
                this.tweens.add({
                    targets: this._counter_reindeer,
                    ease: "Sine.easeOut",
                    duration: 300,
                    delay: 0,
                    repeat: 0,
                    yoyo: true,
                    y: this._counter_reindeer.y - this._counter_reindeer.displayHeight / 2
                })
                this._progressLineRight.setCrop(0, 0, this._lineSize - cropAmount, 98)
            }
        })
        moveTimeline.add({
            targets: this._counter_reindeer,
            ease: "Sine.easeOut",
            duration: 600,
            delay: 0,
            repeat: 0,
            x: this._counter_reindeer.x - picAmount
        })
        moveTimeline.play()
    }

    _initProgressCyclop() {
        let left = this._p._score[0].wins
        let picAmount = this._counter_cyclop.x + ((this.x * 35) / 4) * left
        let cropAmount = (this._lineSize / 4) * left
        this._counter_cyclop.setPosition(picAmount, this._counter_cyclop.y)
        this._progressLineLeft.setCrop(cropAmount, 0, 742, 98)
    }

    _initProgressReindeer() {
        let right = this._p._score[1].wins
        let picAmount = this._counter_reindeer.x - ((this.x * 35) / 4) * right
        let cropAmount = (this._lineSize / 4) * right
        this._counter_reindeer.setPosition(picAmount, this._counter_reindeer.y)
        this._progressLineRight.setCrop(0, 0, this._lineSize - cropAmount, 98)
    }

    _destroy() {
        for (let i in Object.values(this._imgRef)) {
            Object.values(this._imgRef)[i].destroy()
        }
        for (let i in Object.values(this._soundRef)) {
            Object.values(this._soundRef)[i].destroy()
        }
        this._cyclop.destroy()
        this._cyclop = null
        this._reindeer.destroy()
        this._reindeer = null
        this._tutorial1.destroy()
        this._tutorial2.destroy()
        this._tutorial3.destroy()
        this._goCyclops.destroy()
        this._goReindeer.destroy()
        this._soundRef = {}
        this.sound.sounds = []
        this._imgRef = {}
    }
}
