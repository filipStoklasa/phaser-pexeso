import tutorial1 from "./assets/sound/tutorial1.mp3"
import tutorial2 from "./assets/sound/tutorial2.mp3"
import soundOn from "./assets/ui/sound_button.png"
import soundOff from "./assets/ui/sound_disabled_button.png"

export default class MenuScene extends Phaser.Scene {
    constructor() {
        super("MenuScene")
    }
    init(data) {
        console.clear()
        this._skip = data.skip
        this.x = this.game.config.width / 100
        this.y = this.game.config.height / 100
        this._bgRatio = 3328 / 1536
        this._imgSize = this.y * 36
        this._innerPartSize = ((this._bgRatio * (this.y * 100)) / 100) * 54.567307692307686
        this._imgRef = {}
        this.timerEvents = {}
        this._isSoundOn = false
        this._isPlayed = false

        this.add
            .image(this.x * 50, this.y * 50, "bgMain")
            .setOrigin(0.5, 0.5)
            .setDisplaySize(this._bgRatio * (this.y * 100), this.y * 100)

        for (let i = 0; i < 3; i++) {
            this._imgRef[i] = this.add
                .image(this.x * 50 + (i * this._imgSize + i * 10) - this._imgSize / 2 - 10, this.y * 50, `deck${i + 1}`)
                .setDisplaySize(this.y * 36, this.y * 36)
                .setOrigin(1, 0.5)
                .on("pointerdown", this._leaveScene(i))
                .setInteractive()
        }
    }
    preload() {
        this.load.audio("tutorial1", tutorial1)
        this.load.audio("tutorial2", tutorial2)
        this.load.image("soundOff", soundOff)
        this.load.image("soundOn", soundOn)
    }

    create() {
        this.timedEvent = this.time.addEvent({ delay: 5000, callback: this._idleAnimationTimer, callbackScope: this, repeat: -1 })
        this._tutorial1 = this.sound.add("tutorial1")
        this._tutorial2 = this.sound.add("tutorial2")

        this.soundButton = this.add
            .sprite(this.x * 100 - 100, 100, "soundOff")
            .setOrigin(0, 1)
            .setScale(0.5)

        this.soundButton.on("pointerdown", this._playTutorial()).setInteractive()
    }
    _playTutorial() {
        return () => {
            if (this._isSoundOn) {
                this.soundButton.setTexture("soundOff")
                this._isSoundOn = false
            } else {
                this.soundButton.setTexture("soundOn")
                this._isSoundOn = true
                if (!this._isPlayed) {
                    this._isPlayed = true
                    this._tutorial1.play()
                    this.time.delayedCall(
                        Math.ceil(this._tutorial1.duration) * 1000,
                        function() {
                            this._tutorial2.play()
                        },
                        [],
                        this
                    )
                }
            }
        }
    }
    _idleAnimationTimer() {
        for (let i = 0; i < 3; i++) {
            this._idleAnimation(this._imgRef[i], i)
        }
    }
    _idleAnimation(item, index) {
        let idleAnim = this.tweens.createTimeline()
        idleAnim.add({
            targets: item,
            ease: "Power1",
            duration: 200,
            repeat: 0,
            delay: index * 200,
            displayWidth: item.displayWidth * 1.1,
            displayHeight: item.displayWidth * 1.1,
            yoyo: true
        })
        idleAnim.play()
    }

    _leaveScene(index) {
        return () => {
            for (let i in Object.values(this._imgRef)) {
                Object.values(this._imgRef)[i].destroy()
            }
            this.timedEvent.destroy()
            this._tutorial1.destroy()
            this._tutorial2.destroy()
            this.soundButton.destroy()

            if (this._skip) {
                this.scene.start("GameScene", { world: index })
            } else {
                this.scene.start("IntroScene", { world: index })
            }
        }
    }
}
