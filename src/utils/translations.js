export default class Trans {
    constructor(lang) {
        this._lang = lang
        this._transObj = {
            cs: {
                selectTitle: "Úrovně",
                selectDetail: "Úrověň",
                select0: "Úrovně 1-6",
                select1: "Úrovně 7-12",
                select2: "Úrovně 13-18",
                select3: "Úrovně 19-24",
                select4: "Úrovně 25-30"
            },

            en: {
                selectTitle: "Levels",
                selectDetail: "Level",
                select0: "Levels 1-6",
                select1: "Levels 7-12",
                select2: "Levels 13-18",
                select3: "Levels 19-24",
                select4: "Levels 25-30"
            }
        }
    }
    setLanguage(lang) {
        this._lang = lang
    }
    get(key) {
        return this._transObj[this._lang][key]
    }
}
