export default class Card extends Phaser.GameObjects.Image {
    constructor(scene, props) {
        super(scene, props)
        const { src, x, y, imgSize } = props
        this._scene = scene
        this.played = false
        this.setTexture(src)
        this.setPosition(x, y)
        this.setDisplaySize(imgSize, imgSize)
        this.flipCard = this.flip
        this.flipCardBack = this.flipBack
        this.win = this.win
    }

    flip() {
        var timelineBack = this._scene.tweens.createTimeline()
        timelineBack.add({
            targets: this,
            ease: "Power1",
            duration: 300,
            repeat: 0,
            delay: 0,
            scaleX: 0,
            onComplete: () => {
                this.setTexture(this.cardName)
            }
        })
        timelineBack.add({
            targets: this,
            ease: "Power1",
            duration: 300,
            repeat: 0,
            delay: 0,
            scaleX: this.scaleY
        })
        timelineBack.play()
    }

    flipBack() {
        var timelineFlip = this._scene.tweens.createTimeline()
        timelineFlip.add({
            targets: this,
            ease: "Power1",
            duration: 300,
            repeat: 0,
            delay: 0,
            scaleX: 0,
            onComplete: () => {
                this.setTexture(this._scene._p._currentPlayer == 0 ? "card_back_p1" : "card_back_p2")
            }
        })
        timelineFlip.add({
            targets: this,
            ease: "Power1",
            duration: 300,
            repeat: 0,
            delay: 0,
            scaleX: this.scaleY
        })
        timelineFlip.play()
    }

    win(play, rotate) {
        let winCard = this._scene.tweens.createTimeline()
        winCard.add({
            targets: this,
            ease: "Power1",
            duration: 300,
            repeat: 0,
            delay: 0,
            x: this._scene.x * 50,
            y: this._scene.y * 50,
            scaleX: 0.4,
            scaleY: 0.4,
            onComplete: () => {
                if (play) {
                    this._scene._soundRef[this.cardName].play()
                }
                this._scene._selectedCouple = []
            }
        })
        winCard.add({
            targets: this,
            ease: "Power1",
            duration: 300,
            repeat: 0,
            delay: 1000,
            scaleX: this.scaleX * 0.5,
            scaleY: this.scaleY * 0.5,
            rotation: rotate,
            x: this._scene._p._currentPlayer == 0 ? this._scene.x + this.displayWidth / 3 : this._scene.x * 100 - this.displayWidth / 3,
            y: this._scene.y * 50 - this.displayWidth / 3,
            onComplete: () => this._scene._enableAllButtons()
        })
        winCard.play()
    }
}
