export default class Players {
    constructor() {
        this._score = [{ name: "Player1", wins: 0, score: 0 }, { name: "Player2", wins: 0, score: 0 }]
        this._currentPlayer = ~~(Math.random() * 10) > 5 ? 1 : 0
        this._round = 0
    }

    setScore() {
        this._score[this._currentPlayer].score += 1
    }

    changePlayer() {
        this._currentPlayer = (this._currentPlayer + 1) % 2
    }

    weHaveWinner() {
        let winner = false
        for (let i in this._score) {
            if (this._score[i].wins == 4) {
                winner = []
                winner.push(this._score[i].name)
            }
        }
        return winner
    }

    resetInstance() {
        this._score = [{ name: "Player1", wins: 0, score: 0 }, { name: "Player2", wins: 0, score: 0 }]
        this._currentPlayer = ~~(Math.random() * 10) > 5 ? 1 : 0
        this._round = 0
    }

    endOfRound() {
        let winner
        if (this._score[0].score > this._score[1].score) {
            winner = this._score[0].name
            this._score[0].wins++
        } else if (this._score[0].score < this._score[1].score) {
            winner = this._score[1].name
            this._score[1].wins++
        } else {
            winner = "both"
            this._score[0].wins++
            this._score[1].wins++
        }
        this._round++
        this._score[0].score = 0
        this._score[1].score = 0
        return winner
    }
}
