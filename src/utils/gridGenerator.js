import { importGrid } from "./imports"

export default class GridGenerator {
    constructor(grid, type) {
        this._gridWidth = grid.x
        this._gridHeight = grid.y
        this._amount = (grid.x * grid.y) / 2
        this._imports = importGrid(type)
        this.composition = {}

        let i = 0
        while (i <= this._amount - 1) {
            let rand = Math.floor(Math.random() * Object.keys(this._imports).length)
            if (Object.keys(this.composition).indexOf(Object.keys(this._imports)[rand]) == -1) {
                this.composition[Object.keys(this._imports)[rand]] = Object.values(this._imports)[rand]
                i++
            }
        }
    }
}
