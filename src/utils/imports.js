export const importGrid = type => {
    switch (type) {
        case 0:
            return {
                bath: require("../assets/cards/activity/activity_bath.png"),
                cook: require("../assets/cards/activity/activity_cook.png"),
                cycle: require("../assets/cards/activity/activity_cycle.png"),
                dance: require("../assets/cards/activity/activity_dance.png"),
                eat: require("../assets/cards/activity/activity_eat.png"),
                football: require("../assets/cards/activity/activity_football.png"),
                paint: require("../assets/cards/activity/activity_paint.png"),
                play: require("../assets/cards/activity/activity_play.png"),
                read: require("../assets/cards/activity/activity_read.png"),
                relax: require("../assets/cards/activity/activity_relax.png"),
                skate: require("../assets/cards/activity/activity_skate.png"),
                sledge: require("../assets/cards/activity/activity_sledge.png"),
                sleep: require("../assets/cards/activity/activity_sleep.png"),
                swim: require("../assets/cards/activity/activity_swim.png"),
                wash: require("../assets/cards/activity/activity_wash_dishes.png"),
                waterplants: require("../assets/cards/activity/activity_water_plants.png")
            }
        case 1:
            return {
                cyclop: require("../assets/cards/character/character_cyclop.png"),
                dragon: require("../assets/cards/character/character_dragon.png"),
                dwarf: require("../assets/cards/character/character_dwarf.png"),
                fairy: require("../assets/cards/character/character_fairy.png"),
                frog: require("../assets/cards/character/character_frog.png"),
                giant: require("../assets/cards/character/character_giant.png"),
                jester: require("../assets/cards/character/character_jester.png"),
                king: require("../assets/cards/character/character_king.png"),
                knights: require("../assets/cards/character/character_knights.png"),
                peasant: require("../assets/cards/character/character_peasant.png"),
                prince: require("../assets/cards/character/character_prince.png"),
                princess: require("../assets/cards/character/character_princess.png"),
                queen: require("../assets/cards/character/character_queen.png"),
                reindeer: require("../assets/cards/character/character_reindeer.png"),
                unicorn: require("../assets/cards/character/character_unicorn.png"),
                witch: require("../assets/cards/character/character_witch.png")
            }
        case 2:
            return {
                angry: require("../assets/cards/mood/mood_angry.png"),
                bored: require("../assets/cards/mood/mood_bored.png"),
                disgusted: require("../assets/cards/mood/mood_disgusted.png"),
                excited: require("../assets/cards/mood/mood_excited.png"),
                happy: require("../assets/cards/mood/mood_happy.png"),
                hungry: require("../assets/cards/mood/mood_hungry.png"),
                hurt: require("../assets/cards/mood/mood_hurt.png"),
                in_love: require("../assets/cards/mood/mood_in_love.png"),
                nervous: require("../assets/cards/mood/mood_nervous.png"),
                sad: require("../assets/cards/mood/mood_sad.png"),
                scared: require("../assets/cards/mood/mood_scared.png"),
                shy: require("../assets/cards/mood/mood_shy.png"),
                sick: require("../assets/cards/mood/mood_sick.png"),
                silly: require("../assets/cards/mood/mood_silly.png"),
                sleepy: require("../assets/cards/mood/mood_sleepy.png"),
                surprised: require("../assets/cards/mood/mood_surprised.png")
            }
        default:
            return new Error("No type")
    }
}
export const soundImporter = type => {
    switch (type) {
        case 0:
            return {
                bath: require("../assets/sound/activity/bath.mp3"),
                cook: require("../assets/sound/activity/cook.mp3"),
                cycle: require("../assets/sound/activity/cycle.mp3"),
                dance: require("../assets/sound/activity/dance.mp3"),
                eat: require("../assets/sound/activity/eat.mp3"),
                football: require("../assets/sound/activity/football.mp3"),
                paint: require("../assets/sound/activity/paint.mp3"),
                play: require("../assets/sound/activity/play.mp3"),
                read: require("../assets/sound/activity/read.mp3"),
                relax: require("../assets/sound/activity/relax.mp3"),
                skate: require("../assets/sound/activity/skate.mp3"),
                sledge: require("../assets/sound/activity/sledge.mp3"),
                sleep: require("../assets/sound/activity/sleep.mp3"),
                swim: require("../assets/sound/activity/swim.mp3"),
                wash: require("../assets/sound/activity/wash.mp3"),
                waterplants: require("../assets/sound/activity/waterplants.mp3")
            }
        case 1:
            return {
                cyclop: require("../assets/sound/character/cyclop.mp3"),
                dragon: require("../assets/sound/character/dragon.mp3"),
                dwarf: require("../assets/sound/character/dwarf.mp3"),
                fairy: require("../assets/sound/character/fairy.mp3"),
                frog: require("../assets/sound/character/frog.mp3"),
                giant: require("../assets/sound/character/giant.mp3"),
                jester: require("../assets/sound/character/jester.mp3"),
                king: require("../assets/sound/character/king.mp3"),
                knights: require("../assets/sound/character/knights.mp3"),
                peasant: require("../assets/sound/character/peasant.mp3"),
                prince: require("../assets/sound/character/prince.mp3"),
                princess: require("../assets/sound/character/princess.mp3"),
                queen: require("../assets/sound/character/queen.mp3"),
                reindeer: require("../assets/sound/character/reindeer.mp3"),
                unicorn: require("../assets/sound/character/unicorn.mp3"),
                witch: require("../assets/sound/character/witch.mp3")
            }
        case 2:
            return {
                angry: require("../assets/sound/mood/angry.mp3"),
                bored: require("../assets/sound/mood/bored.mp3"),
                disgusted: require("../assets/sound/mood/disgusted.mp3"),
                excited: require("../assets/sound/mood/excited.mp3"),
                happy: require("../assets/sound/mood/happy.mp3"),
                hungry: require("../assets/sound/mood/hungry.mp3"),
                hurt: require("../assets/sound/mood/hurt.mp3"),
                in_love: require("../assets/sound/mood/in_love.mp3"),
                nervous: require("../assets/sound/mood/nervous.mp3"),
                sad: require("../assets/sound/mood/sad.mp3"),
                scared: require("../assets/sound/mood/scared.mp3"),
                shy: require("../assets/sound/mood/shy.mp3"),
                sick: require("../assets/sound/mood/sick.mp3"),
                silly: require("../assets/sound/mood/silly.mp3"),
                sleepy: require("../assets/sound/mood/sleepy.mp3"),
                surprised: require("../assets/sound/mood/surprised.mp3")
            }
        default:
            return new Error("no level selected")
    }
}
