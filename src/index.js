import Phaser from "phaser"
import Preloader from "./Preloader"
import SelectScene from "./SelectScene"
import MenuScene from "./MenuScene"
import GameScene from "./GameScene"
import ProgressScene from "./ProgressScene"
import IntroScene from "./IntroScene"
import Players from "./utils/Players"

const config = {
    type: Phaser.AUTO,
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    parent: "phaser-example",
    width: window.innerWidth,
    height: window.innerHeight,
    autoResize: true,
    backgroundColor: "#042C33",
    scene: [Preloader, MenuScene, IntroScene, SelectScene, GameScene, ProgressScene],
    plugins: {
        scene: [
            {
                key: "SpineWebGLPlugin",
                plugin: SpineWebGLPlugin,
                start: true,
                sceneKey: "spine"
            }
        ]
    }
}

const game = new Phaser.Game(config)
game.players = new Players()
